#![feature(test)]


extern crate test;

extern crate immut_list;
extern crate data_structure_traits;


use test::Bencher;

use immut_list::ImmutList;
use std::collections::LinkedList;
use data_structure_traits::*;


const SIZE: usize = 1024 * 8;


#[bench]
fn bench_immut_list(b: &mut Bencher) {
    b.iter(|| {
        let mut a = ImmutList::new();

        for i in 0..SIZE {
            a = a.push(i);
        }

        a
    });
}
#[bench]
fn bench_std_linked_list(b: &mut Bencher) {
    b.iter(|| {
        let mut a = LinkedList::new();

        for i in 0..SIZE {
            a.push_front(i);
        }
        a
    });
}

#[bench]
fn bench_immut_list_iter(b: &mut Bencher) {
    let mut a = ImmutList::new();
    let mut index = SIZE;

    for i in 0..SIZE {
        a = a.push(i);
    }

    b.iter(move || {
        index = SIZE;
        for i in a.iter() {
            index -= 1;
            assert_eq!(i, &index);
        }
    });
}
#[bench]
fn bench_std_linked_list_iter(b: &mut Bencher) {
    let mut a = LinkedList::new();
    let mut index = SIZE;

    for i in 0..SIZE {
        a.push_front(i);
    }

    b.iter(move || {
        index = SIZE;
        for i in a.iter() {
            index -= 1;
            assert_eq!(i, &index);
        }
    });
}
