use alloc::arc::Arc;

use core::marker::PhantomData;

use super::immut_list_node::ImmutListNode;


#[derive(Clone)]
pub struct ImmutListIter<'a, T>
    where T: 'a,
{
    root: &'a Option<Arc<ImmutListNode<T>>>,
    len: usize,
    phantom_data: PhantomData<&'a Arc<ImmutListNode<T>>>,
}

unsafe impl<'a, T: 'a> Send for ImmutListIter<'a, T> {}
unsafe impl<'a, T: 'a> Sync for ImmutListIter<'a, T> {}

impl<'a, T> ImmutListIter<'a, T>
    where T: 'a,
{
    #[inline(always)]
    pub fn new(root: &'a Option<Arc<ImmutListNode<T>>>, len: usize) -> Self {
        ImmutListIter {
            root: root,
            len: len,
            phantom_data: PhantomData,
        }
    }
}

impl<'a, T> Iterator for ImmutListIter<'a, T>
    where T: 'a,
{
    type Item = &'a T;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if self.len == 0 {
            None
        } else {
            self.root.as_ref().map(|root| {
                self.root = &root.next;
                self.len -= 1;
                &*root.data
            })
        }
    }

    #[inline(always)]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<'a, T> ExactSizeIterator for ImmutListIter<'a, T>
    where T: 'a,
{
    #[inline(always)]
    fn len(&self) -> usize {
        self.len
    }
}
