use alloc::arc::Arc;

use core::iter::FromIterator;
use core::fmt;

use data_structure_traits::*;

use super::immut_list_node::ImmutListNode;
use super::{ImmutListBuilder, ImmutListIter};

#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ImmutList<T> {
    root: Option<Arc<ImmutListNode<T>>>,
    len: usize,
}

unsafe impl<T> Send for ImmutList<T> {}
unsafe impl<T> Sync for ImmutList<T> {}

impl<T> ImmutList<T> {
    #[inline(always)]
    pub const fn new() -> Self {
        Self::from_root_and_len(None, 0)
    }

    #[inline(always)]
    pub(crate) const fn from_root_and_len(root: Option<Arc<ImmutListNode<T>>>, len: usize) -> Self {
        ImmutList {
            root: root,
            len: len,
        }
    }
}

impl<T> From<ImmutListBuilder<T>> for ImmutList<T> {
    #[inline(always)]
    fn from(builder: ImmutListBuilder<T>) -> Self {
        builder.build()
    }
}

impl<T> fmt::Debug for ImmutList<T>
    where T: fmt::Debug,
{
    #[inline(always)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list().entries(self).finish()
    }
}

impl<T> fmt::Display for ImmutList<T>
    where T: fmt::Debug + fmt::Display,
{
    #[inline(always)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list().entries(self).finish()
    }
}

impl<T> Default for ImmutList<T> {
    #[inline(always)]
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Collection for ImmutList<T> {
    #[inline(always)]
    fn len(&self) -> usize { self.len }
}

impl<T> Create<T> for ImmutList<T> {
    #[inline(always)]
    fn create() -> Self { Self::new() }

    #[inline(always)]
    fn create_with_capacity(_: usize) -> Self { Self::new() }

    #[inline(always)]
    fn add_element(mut self, element: T) -> Self {
        self.root = Some(Arc::new(ImmutListNode::new(Arc::new(element), self.root)));
        self.len += 1;
        self
    }
}

impl<T> StackImmut<T> for ImmutList<T> {
    #[inline]
    fn push(&self, data: T) -> Self {
        ImmutList {
            root: Some(Arc::new(ImmutListNode::new(Arc::new(data), self.root.clone()))),
            len: self.len + 1,
        }
    }

    #[inline]
    fn pop(&self) -> Self {
        match &self.root {
            &Some(ref root) => {
                ImmutList {
                    root: root.next.clone(),
                    len: self.len - 1,
                }
            },
            &None => Self::new(),
        }
    }

    #[inline]
    fn top(&self) -> Option<&T> {
        match &self.root {
            &Some(ref root) => Some(&*root.data),
            &None => None,
        }
    }
}

impl<T> Get<usize> for ImmutList<T> {
    type Output = T;

    #[inline]
    fn get(&self, index: usize) -> Option<&Self::Output> {
        let mut node = &self.root;
        let mut i = 0;

        while let &Some(ref n) = node {
            if i == index {
                return Some(n.data.as_ref())
            }
            node = &n.next;
            i += 1;
        }

        None
    }
}

impl<'a, T> ImmutList<T>
    where T: 'a,
{
    #[inline(always)]
    pub fn iter(&'a self) -> ImmutListIter<'a, T> {
        ImmutListIter::new(&self.root, self.len)
    }
}

impl<'a, T> IntoIterator for &'a ImmutList<T>
    where T: 'a,
{
    type Item = &'a T;
    type IntoIter = ImmutListIter<'a, T>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<T> FromIterator<T> for ImmutList<T> {
    #[inline(always)]
    fn from_iter<I>(iter: I) -> Self
        where I: IntoIterator<Item = T>,
    {
        ImmutListBuilder::from_iter(iter).build()
    }
}


#[cfg(test)]
mod test {
    use super::*;


    #[test]
    fn test_push_pop() {
        let a = ImmutList::new();
        let b = a.push(1);
        let c = b.push(2);
        let d = c.pop();

        assert_eq!(a.len(), 0);

        assert_eq!(*b.top().unwrap(), 1);
        assert_eq!(b.len(), 1);

        assert_eq!(*c.top().unwrap(), 2);
        assert_eq!(c.len(), 2);

        assert_eq!(*d.top().unwrap(), 1);
        assert_eq!(d.len(), 1);
    }

    #[test]
    fn test_iter() {
        let a = ImmutList::new();
        let b = a.push(3);
        let c = b.push(2);
        let d = c.push(1);
        let e = d.push(0);
        let mut i = 0;

        for value in e.iter() {
            assert_eq!(*value, i);
            i += 1;
        }
    }
}
