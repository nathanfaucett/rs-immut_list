use alloc::arc::Arc;

use core::iter::FromIterator;

use super::immut_list_node::ImmutListNode;
use super::{ImmutListIter, ImmutList};


#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ImmutListBuilder<T> {
    root: Option<Arc<ImmutListNode<T>>>,
    len: usize,
}

impl<T> ImmutListBuilder<T> {
    #[inline(always)]
    pub const fn new() -> Self {
        ImmutListBuilder {
            root: None,
            len: 0usize,
        }
    }

    #[inline]
    pub fn push(mut self, data: T) -> Self {
        self.root = Some(Arc::new(ImmutListNode::new(Arc::new(data), self.root)));
        self.len += 1;
        self
    }

    #[inline(always)]
    pub fn build(self) -> ImmutList<T> {
        ImmutList::from_root_and_len(self.root, self.len)
    }
}

impl<'a, T> ImmutListBuilder<T>
    where T: 'a,
{
    #[inline(always)]
    pub fn iter(&'a self) -> ImmutListIter<'a, T> {
        ImmutListIter::new(&self.root, self.len)
    }
}

impl<'a, T> IntoIterator for &'a ImmutListBuilder<T>
    where T: 'a,
{
    type Item = &'a T;
    type IntoIter = ImmutListIter<'a, T>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<T> FromIterator<T> for ImmutListBuilder<T> {
    #[inline]
    fn from_iter<I>(iter: I) -> Self
        where I: IntoIterator<Item = T>,
    {
        let mut builder = ImmutListBuilder::new();
        for value in iter {
            builder = builder.push(value);
        }
        builder
    }
}
