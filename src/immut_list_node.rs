use alloc::arc::Arc;


#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ImmutListNode<T> {
    pub(crate) data: Arc<T>,
    pub(crate) next: Option<Arc<ImmutListNode<T>>>,
}

impl<T> ImmutListNode<T> {
    #[inline(always)]
    pub fn new(data: Arc<T>, next: Option<Arc<ImmutListNode<T>>>) -> Self {
        ImmutListNode {
            data: data,
            next: next,
        }
    }
}
