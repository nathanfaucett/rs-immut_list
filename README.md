immut_list
=====

immutable persistent list

```rust
extern crate immut_list;
extern crate data_structure_traits;


use immut_list::ImmutList;
use data_structure_traits::*;


fn main() {
    let mut a = ImmutList::new();

    for i in 0..32 {
        a = a.push(i);
    }

    let mut index = 32;
    for i in &a {
        index -= 1;
        assert_eq!(i, &index);
    }

    println!("{:?}", a);
}
```
